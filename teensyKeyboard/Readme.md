# OSCKeyboard for MadMini


##Messages

* `/play` + an int as parameter between 0 and 9.
* `/restartMovie`
* `/pauseMovie`
* `/nextMovie`
* `/previousMovie`
* `/displayInfos`
* `/changePlayBackMode`
* `/increaseImageDisplayTime`
* `/decreaseImageDisplayTime`
* `/changeAudioOutput`
* `/showHelp`
* `/shutdown`

##Usage

1. Plug the USB Cable to your MiniMad while it's still powered off
2. Power it
3. Once the video appear you can send a message or a bundle to your Minimad.
4. default address is : 192.168.8.100:1234 but you can change it in the code.
