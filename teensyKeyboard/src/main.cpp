#include <Arduino.h>

//#define DEBUG

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>


#include <OSCBundle.h>
#include <OSCBoards.h>


const unsigned int inPort = 1234;
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xAA, 0xED
};
IPAddress ip(192, 168, 8, 100);
EthernetUDP Udp;



void play(OSCMessage &msg, int addrOffset );
void printIP();
void startEthernet();
void feedback(OSCMessage inMessage);
int getMessageFirstParam(OSCMessage &msg);

//KEYSTROKES
void sendAndRelease();
void pressNumber(uint8_t key);
void backspace(OSCMessage &msg, int addrOffset );
void space( OSCMessage &msg, int addrOffset );
void rightArrow( OSCMessage &msg, int addrOffset );
void leftArrow( OSCMessage &msg, int addrOffset );
void letterD( OSCMessage &msg, int addrOffset );
void letterP( OSCMessage &msg, int addrOffset );
void upArrow( OSCMessage &msg, int addrOffset );
void downArrow( OSCMessage &msg, int addrOffset );
void letterA( OSCMessage &msg, int addrOffset );
void letterH( OSCMessage &msg, int addrOffset );
void letterQ( OSCMessage &msg, int addrOffset );




void setup()
{
  #ifdef DEBUG
  Serial.begin(115200);
  #endif
  Keyboard.begin();
  delay(2000);
  startEthernet();
}

void loop()
{
  OSCBundle bundleIN;
  OSCMessage messageIN;
  int size;
  if( (size = Udp.parsePacket())>0 )
  {
    Serial.println("received OSC");
    while(size--)
    {
      char c = Udp.read();
      messageIN.fill(c);
      bundleIN.fill(c);
    }

    if(!bundleIN.hasError())
    {
      bundleIN.route("/play", play);
      bundleIN.route("/restartMovie",backspace);
      bundleIN.route("/pauseMovie",space);
      bundleIN.route("/nextMovie",rightArrow);
      bundleIN.route("/previousMovie",leftArrow);
      bundleIN.route("/displayInfos",letterD);
      bundleIN.route("/changePlayBackMode",letterP);
      bundleIN.route("/increaseImageDisplayTime",upArrow);
      bundleIN.route("/decreaseImageDisplayTime",downArrow);
      bundleIN.route("/changeAudioOutput",letterA);
      bundleIN.route("/showHelp",letterH);
      bundleIN.route("/shutdown",letterQ);

      #ifdef DEBUG
      feedback(bundleIN.getOSCMessage(0));
      #endif
    }

    if(!messageIN.hasError())
    {
      messageIN.route("/play", play);
      messageIN.route("/restartMovie",backspace);
      messageIN.route("/pauseMovie",space);
      messageIN.route("/nextMovie",rightArrow);
      messageIN.route("/previousMovie",leftArrow);
      messageIN.route("/displayInfos",letterD);
      messageIN.route("/changePlayBackMode",letterP);
      messageIN.route("/increaseImageDisplayTime",upArrow);
      messageIN.route("/decreaseImageDisplayTime",downArrow);
      messageIN.route("/changeAudioOutput",letterA);
      messageIN.route("/showHelp",letterH);
      messageIN.route("/shutdown",letterQ);
      #ifdef DEBUG
      feedback(messageIN);
      #endif
    }

  }
}

void feedback(OSCMessage inMessage)
{
  OSCBundle bundle;
  bundle.add(inMessage);
  Udp.beginPacket("192.168.8.255", 1234);
  bundle.send(Udp);
  Udp.endPacket();
  bundle.empty();
}

int getMessageFirstParam(OSCMessage &msg)
{
  int value = -1;
  if (msg.isInt(0))
  {
    value = msg.getInt(0);
  }
  else if(msg.isFloat(0))
  {
    value = floor(msg.getFloat(0));
  }
  else
  {
    value = 1;
    Serial.println("Message has no param, sending keystroke");
  }
  return value;
}
void play(OSCMessage &msg, int addrOffset )
{
  int key = -1;
  if (msg.isInt(0))
  {
    key = msg.getInt(0);
  }
  else if(msg.isFloat(0))
  {
    key = floor(msg.getFloat(0));
  }
  pressNumber(key);
}

void backspace( OSCMessage &msg, int addrOffset )
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_BACKSPACE);
    sendAndRelease();
  }
  #else
  Serial.print("BACKSPACE");
  #endif
}

void space( OSCMessage &msg, int addrOffset )
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_SPACE);
    sendAndRelease();
  }
  #else
  Serial.print("SPACE");
  #endif
}

void rightArrow( OSCMessage &msg, int addrOffset )
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_RIGHT);
    sendAndRelease();
  }
  #else
  Serial.print("RIGHT ARROW");
  #endif
}

void leftArrow( OSCMessage &msg, int addrOffset )
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_LEFT);
    sendAndRelease();
  }
  #else
  Serial.print("LEFT ARROW");
  #endif
}

void letterD( OSCMessage &msg, int addrOffset )//displayInfos
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_D);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_D");
  #endif
}


void letterP( OSCMessage &msg, int addrOffset )//CHANGE PLAYBACK MODE
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_P);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_P");
  #endif
}

void upArrow( OSCMessage &msg, int addrOffset )//INCREASE DISPLAY TIME
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_UP);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_UP");
  #endif
}

void downArrow( OSCMessage &msg, int addrOffset )//DECREASE DISPLAY TIME
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_DOWN);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_DOWN");
  #endif
}

void letterA( OSCMessage &msg, int addrOffset )//CHANGE AUDIO OUTPUT
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_A);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_A");
  #endif
}

void letterH( OSCMessage &msg, int addrOffset )//TOOGLE HELP
{
  #ifndef DEBUG
  if (getMessageFirstParam(msg) == 1)
  {
    Keyboard.set_key1(KEY_H);
    sendAndRelease();
  }
  #else
  Serial.print("KEY_H");
  #endif
}

void letterQ( OSCMessage &msg, int addrOffset )//SHUTDOWN
{
  #ifndef DEBUG
  Keyboard.set_key1(KEY_Q);
  Keyboard.send_now();//hold to quit
  #else
  Serial.print("KEY_Q");
  #endif
}

void startEthernet()
{
  Serial.println("Initialize Ethernet :");
  pinMode(9, OUTPUT);
  digitalWrite(9, LOW);    // begin reset the WIZ820io
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);  // de-select WIZ820io
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);   // de-select the SD Card
  digitalWrite(9, HIGH);
  Ethernet.begin(mac,ip) ;
  Udp.begin(inPort);
  Serial.println("connected...");
  printIP();
}

void pressNumber(uint8_t key)
{
  #ifndef DEBUG
  switch (key)
  {
    case 0:
    Keyboard.set_key1(KEY_0);
    break;
    case 1:
    Keyboard.set_key1(KEY_1);
    break;
    case 2:
    Keyboard.set_key1(KEY_2);
    break;
    case 3:
    Keyboard.set_key1(KEY_3);
    break;
    case 4:
    Keyboard.set_key1(KEY_4);
    break;
    case 5:
    Keyboard.set_key1(KEY_5);
    break;
    case 6:
    Keyboard.set_key1(KEY_6);
    break;
    case 7:
    Keyboard.set_key1(KEY_7);
    break;
    case 8:
    Keyboard.set_key1(KEY_8);
    break;
    case 9:
    Keyboard.set_key1(KEY_9);
    break;
  }
  sendAndRelease();
  #else
  Serial.print("Key pressed :");Serial.println(key);
  #endif
}

void printIP() {
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
}

void sendAndRelease()
{
  #ifndef DEBUG
  Keyboard.send_now();
  delay(100);
  Keyboard.releaseAll();
  delay(100);
  #endif
}
