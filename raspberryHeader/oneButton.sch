EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ameisso
LIBS:raspberryHeader-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SPST SW1
U 1 1 58AC74AE
P 3000 2250
AR Path="/58AC741F/58AC74AE" Ref="SW1"  Part="1" 
AR Path="/58AF467B/58AC74AE" Ref="SW2"  Part="1" 
AR Path="/58AF4775/58AC74AE" Ref="SW3"  Part="1" 
AR Path="/58AF499C/58AC74AE" Ref="SW4"  Part="1" 
AR Path="/58AF49A1/58AC74AE" Ref="SW5"  Part="1" 
AR Path="/58AF49A6/58AC74AE" Ref="SW6"  Part="1" 
AR Path="/58AF4B16/58AC74AE" Ref="SW7"  Part="1" 
AR Path="/58AF4F73/58AC74AE" Ref="SW8"  Part="1" 
AR Path="/58AF5010/58AC74AE" Ref="SW9"  Part="1" 
AR Path="/58AF50CF/58AC74AE" Ref="SW10"  Part="1" 
AR Path="/58AF530E/58AC74AE" Ref="SW11"  Part="1" 
F 0 "SW11" H 2900 2000 50  0000 C CNN
F 1 "SPST" H 2900 1800 50  0000 C CNN
F 2 "ameisso:SPST_SWITCH" H 2900 1900 50  0001 C CNN
F 3 "" H 2900 1900 50  0000 C CNN
	1    3000 2250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 58AC74F6
P 2250 2850
AR Path="/58AC741F/58AC74F6" Ref="R4"  Part="1" 
AR Path="/58AF467B/58AC74F6" Ref="R5"  Part="1" 
AR Path="/58AF4775/58AC74F6" Ref="R6"  Part="1" 
AR Path="/58AF499C/58AC74F6" Ref="R7"  Part="1" 
AR Path="/58AF49A1/58AC74F6" Ref="R8"  Part="1" 
AR Path="/58AF49A6/58AC74F6" Ref="R9"  Part="1" 
AR Path="/58AF4B16/58AC74F6" Ref="R10"  Part="1" 
AR Path="/58AF4F73/58AC74F6" Ref="R11"  Part="1" 
AR Path="/58AF5010/58AC74F6" Ref="R12"  Part="1" 
AR Path="/58AF50CF/58AC74F6" Ref="R13"  Part="1" 
AR Path="/58AF530E/58AC74F6" Ref="R14"  Part="1" 
F 0 "R14" V 2330 2850 50  0000 C CNN
F 1 "10K" V 2250 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 2180 2850 50  0001 C CNN
F 3 "" H 2250 2850 50  0000 C CNN
	1    2250 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 58AC7534
P 2250 3100
AR Path="/58AC741F/58AC7534" Ref="#PWR018"  Part="1" 
AR Path="/58AF467B/58AC7534" Ref="#PWR020"  Part="1" 
AR Path="/58AF4775/58AC7534" Ref="#PWR022"  Part="1" 
AR Path="/58AF499C/58AC7534" Ref="#PWR024"  Part="1" 
AR Path="/58AF49A1/58AC7534" Ref="#PWR026"  Part="1" 
AR Path="/58AF49A6/58AC7534" Ref="#PWR028"  Part="1" 
AR Path="/58AF4B16/58AC7534" Ref="#PWR030"  Part="1" 
AR Path="/58AF4F73/58AC7534" Ref="#PWR032"  Part="1" 
AR Path="/58AF5010/58AC7534" Ref="#PWR034"  Part="1" 
AR Path="/58AF50CF/58AC7534" Ref="#PWR036"  Part="1" 
AR Path="/58AF530E/58AC7534" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 2250 2850 50  0001 C CNN
F 1 "GND" H 2250 2950 50  0000 C CNN
F 2 "" H 2250 3100 50  0000 C CNN
F 3 "" H 2250 3100 50  0000 C CNN
	1    2250 3100
	1    0    0    -1  
$EndComp
Text HLabel 2000 2600 0    60   Input ~ 0
TO_GPIO
Wire Wire Line
	3400 2600 3400 2350
Wire Wire Line
	2000 2600 2400 2600
Wire Wire Line
	2250 2700 2250 2600
Connection ~ 2250 2600
Wire Wire Line
	2250 3100 2250 3000
$Comp
L +3V3 #PWR019
U 1 1 58B2DF30
P 3400 2350
AR Path="/58AC741F/58B2DF30" Ref="#PWR019"  Part="1" 
AR Path="/58AF467B/58B2DF30" Ref="#PWR021"  Part="1" 
AR Path="/58AF4775/58B2DF30" Ref="#PWR023"  Part="1" 
AR Path="/58AF499C/58B2DF30" Ref="#PWR025"  Part="1" 
AR Path="/58AF49A1/58B2DF30" Ref="#PWR027"  Part="1" 
AR Path="/58AF49A6/58B2DF30" Ref="#PWR029"  Part="1" 
AR Path="/58AF4B16/58B2DF30" Ref="#PWR031"  Part="1" 
AR Path="/58AF4F73/58B2DF30" Ref="#PWR033"  Part="1" 
AR Path="/58AF5010/58B2DF30" Ref="#PWR035"  Part="1" 
AR Path="/58AF50CF/58B2DF30" Ref="#PWR037"  Part="1" 
AR Path="/58AF530E/58B2DF30" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 3400 2200 50  0001 C CNN
F 1 "+3V3" H 3400 2490 50  0000 C CNN
F 2 "" H 3400 2350 50  0000 C CNN
F 3 "" H 3400 2350 50  0000 C CNN
	1    3400 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
